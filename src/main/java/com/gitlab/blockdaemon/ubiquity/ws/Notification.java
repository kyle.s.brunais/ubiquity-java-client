package com.gitlab.blockdaemon.ubiquity.ws;

import com.gitlab.blockdaemon.ubiquity.model.Block;
import com.gitlab.blockdaemon.ubiquity.model.BlockIdentifier;
import com.gitlab.blockdaemon.ubiquity.model.Tx;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public abstract class Notification  {
	private String method;
	private boolean revert;
	private int subID;

	@Data
	@EqualsAndHashCode(callSuper=true)
	@NoArgsConstructor
	public static class TxNotification extends Notification {
		private Tx content;
	}

	@Data
	@EqualsAndHashCode(callSuper=true)
	@NoArgsConstructor
	public static class BlockNotification extends Notification {
		private Block content;
	}

	@Data
	@EqualsAndHashCode(callSuper=true)
	@NoArgsConstructor
	public static class BlockIdentifierNotification extends Notification{
		private BlockIdentifier content;
	}
}
