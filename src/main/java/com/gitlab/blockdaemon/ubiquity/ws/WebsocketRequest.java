package com.gitlab.blockdaemon.ubiquity.ws;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class WebsocketRequest {

	public static final String SUBSCRIBE = "ubiquity.subscribe";
	public static final String UNSUBSCRIBE = "ubiquity.unsubscribe";

	private Integer id;
	private String method;
	private Params params;

	@NoArgsConstructor
	@Data
	public static class Params {

		private Integer subID;
		private String channel;
		private Map<String, Object> detail;

		public Params(String channel, Map<String, Object> detail) {
			this.channel = channel;
			this.detail = detail;
		}

		public static Params blockIdentifiers() {
			return blockIdentifiers(new HashMap<String, Object>());
		}

		public static Params blockIdentifiers(Map<String, Object> details) {
			return new Params("ubiquity.block_identifiers", details);
		}

		public static Params blocks() {
			return blocks(new HashMap<String, Object>());
		}

		public static Params blocks(Map<String, Object> details) {
			return new Params("ubiquity.blocks", details);
		}

		public static Params tx() {
			return tx(new HashMap<String, Object>());
		}

		public static Params tx(Map<String, Object> details) {
			return new Params("ubiquity.txs", details);
		}
	}
}
