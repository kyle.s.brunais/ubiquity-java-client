package com.gitlab.blockdaemon.ubiquity.tx.btc;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import com.gitlab.blockdaemon.ubiquity.tx.btc.exceptions.BitcoinException;
import com.gitlab.blockdaemon.ubiquity.tx.btc.util.Base58;
import com.gitlab.blockdaemon.ubiquity.tx.btc.util.Bech32;

public final class Address {
	public static final int TYPE_MAINNET = 0;
	public static final int TYPE_TESTNET = 111;

	public static final int TYPE_P2SH = 5;
	public static final int TYPE_P2SH_TESTNET = 196;
	public static final int TYPE_NONE = -1;

	public enum Type {
		PUBLIC_KEY_TO_ADDRESS_LEGACY, PUBLIC_KEY_TO_ADDRESS_P2WKH, PUBLIC_KEY_TO_ADDRESS_P2SH_P2WKH
	}

	private final Script.WitnessProgram witnessProgram;

	private final int keyhashType;
	private final byte[] hash160;

	private final String addressString;

	public Address(String address) throws BitcoinException {
		if (address == null) {
			throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "Null address");
		}
		if (address.length() > 3) {
			final String prefix = address.substring(0, 2).toLowerCase(Locale.ENGLISH);
			if (prefix.equals("bc") || prefix.equals("tc")) {
				this.witnessProgram = Bech32.decodeSegwitAddress(prefix, address);
				this.hash160 = null;
				this.keyhashType = TYPE_NONE;
				this.addressString = address;
				return;
			}
		}
		final byte[] decodedAddress = Base58.decode(address);
		if (decodedAddress == null || decodedAddress.length < 6) {
			throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "Bad address length");
		}
		this.keyhashType = decodedAddress[0] & 0xff;
		if (this.keyhashType == TYPE_MAINNET || this.keyhashType == TYPE_TESTNET || this.keyhashType == TYPE_P2SH
				|| this.keyhashType == TYPE_P2SH_TESTNET) {
			if (BtcService.verifyDoubleSha256Checksum(decodedAddress)) {
				if (decodedAddress.length != 1 + 20 + 4) {
					throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "HASH160 should be 20 bytes length");
				}
				this.witnessProgram = null;
				this.hash160 = new byte[20];
				System.arraycopy(decodedAddress, 1, this.hash160, 0, this.hash160.length);
			} else {
				throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "Bad address checksum");
			}
		} else {
			throw new BitcoinException(BitcoinException.ERR_WRONG_TYPE,
					"Unsupported address type " + (decodedAddress[0] & 0xff));
		}
		this.addressString = ripemd160HashToAddress((byte) this.keyhashType, this.hash160);
	}

	public Address(boolean testNet, Script.WitnessProgram witnessProgram) throws BitcoinException {
		this.witnessProgram = witnessProgram;
		this.keyhashType = TYPE_NONE;
		this.hash160 = null;
		this.addressString = Bech32.encodeSegwitAddress(testNet ? "bc" : "tc", witnessProgram.version,
				witnessProgram.program);
	}

	@Override
	public String toString() {
		return this.addressString;
	}

	static Address decode(String address) {
		try {
			return new Address(address);
		} catch (final BitcoinException ignored) {
			return null;
		}
	}

	public static boolean verify(String address, boolean acceptSegwit) {
		final Address decodedAddress = decode(address);
		return decodedAddress != null && (acceptSegwit || decodedAddress.witnessProgram == null);
	}

	public static boolean verify(String address) {
		return decode(address) != null;
	}

	static String publicKeyToAddress(byte[] publicKey) {
		return publicKeyToAddress(false, publicKey);
	}

	static String publicKeyToAddress(boolean testNet, byte[] publicKey) {
		return ripemd160HashToAddress(testNet, BtcService.sha256ripemd160(publicKey));
	}

	static String publicKeyToP2wkhAddress(boolean testNet, byte[] publicKey) {
		if (publicKey.length > 33) {
			return null; // not a compressed key
		}
		try {
			return Bech32.encodeSegwitAddress(testNet ? "tc" : "bc", 0, BtcService.sha256ripemd160(publicKey));
		} catch (final BitcoinException unexpected) {
			throw new RuntimeException(unexpected);
		}
	}

	static String publicKeyToP2shP2wkhAddress(boolean testNet, byte[] publicKey) {
		if (publicKey.length > 33) {
			return null; // not a compressed key
		}
		return ripemd160HashToP2shAddress(testNet, BtcService
				.sha256ripemd160(new Script.WitnessProgram(0, BtcService.sha256ripemd160(publicKey)).getBytes()));
	}

	static String ripemd160HashToAddress(boolean testNet, byte[] hashedPublicKey) {
		final byte version = (byte) (testNet ? TYPE_TESTNET : TYPE_MAINNET);
		return ripemd160HashToAddress(version, hashedPublicKey);
	}

	static String ripemd160HashToP2shAddress(boolean testNet, byte[] hashedPublicKey) {
		final byte version = (byte) (testNet ? TYPE_P2SH_TESTNET : TYPE_P2SH);
		return ripemd160HashToAddress(version, hashedPublicKey);
	}

	private static String ripemd160HashToAddress(byte version, byte[] hashedPublicKey) {
		try {
			final byte[] addressBytes = new byte[1 + hashedPublicKey.length + 4];
			addressBytes[0] = version;
			System.arraycopy(hashedPublicKey, 0, addressBytes, 1, hashedPublicKey.length);
			final MessageDigest digestSha = MessageDigest.getInstance("SHA-256");
			digestSha.update(addressBytes, 0, addressBytes.length - 4);
			final byte[] check = digestSha.digest(digestSha.digest());
			System.arraycopy(check, 0, addressBytes, hashedPublicKey.length + 1, 4);
			return Base58.encode(addressBytes);
		} catch (final NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final Address address = (Address) o;
		return this.addressString.equals(address.addressString);
	}

	@Override
	public int hashCode() {
		return this.addressString.hashCode();
	}

	public Script.WitnessProgram getWitnessProgram() {
		return this.witnessProgram;
	}

	public int getKeyhashType() {
		return this.keyhashType;
	}

	public byte[] getHash160() {
		return this.hash160;
	}

	public String getAddressString() {
		return this.addressString;
	}

}
