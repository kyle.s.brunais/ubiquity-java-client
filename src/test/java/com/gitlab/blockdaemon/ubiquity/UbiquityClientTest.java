/*
 * Ubiquity REST API
 * Ubiquity provides a RESTful and uniform way to access blockchain resources, with a rich and reusable model across multiple cryptocurrencies.  [Documentation](https://app.blockdaemon.com/docs/ubiquity)  ##### Pagination Certain resources contain a lot of data, more than what's practical to return for a single request. With the help of pagination, the data is split across multiple responses. Each response returns a subset of the items requested and a continuation token.  To get the next batch of items, copy the returned continuation token to the continuation query parameter and repeat the request with the new URL. In case no continuation token is returned, there is no more data available. 
 *
 * The version of the OpenAPI document: 2.0.0
 * Contact: support@blockdaemon.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

package com.gitlab.blockdaemon.ubiquity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static com.gitlab.blockdaemon.ubiquity.TestUtil.readJsonFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.model.Block;
import com.gitlab.blockdaemon.ubiquity.model.MultiTransfer;
import com.gitlab.blockdaemon.ubiquity.model.NativeCurrency;
import com.gitlab.blockdaemon.ubiquity.model.Network;
import com.gitlab.blockdaemon.ubiquity.model.Order;
import com.gitlab.blockdaemon.ubiquity.model.Platform;
import com.gitlab.blockdaemon.ubiquity.model.PlatformDetail;
import com.gitlab.blockdaemon.ubiquity.model.PlatformsOverview;
import com.gitlab.blockdaemon.ubiquity.model.PlatformsOverviewPlatforms;
import com.gitlab.blockdaemon.ubiquity.model.SmartTokenCurrency;
 
import com.gitlab.blockdaemon.ubiquity.model.Transfer;
import com.gitlab.blockdaemon.ubiquity.model.Tx;
import com.gitlab.blockdaemon.ubiquity.model.TxPage;
import com.gitlab.blockdaemon.ubiquity.model.Utxo;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;


public class UbiquityClientTest {

	public static MockWebServer mockBackEnd;
	public static String URL;
	private static UbiquityClient client;
	
	@BeforeClass
	public static void setUp() throws IOException {
		mockBackEnd = new MockWebServer();
		mockBackEnd.start();
		URL = String.format("http://%s:%s", mockBackEnd.getHostName(), mockBackEnd.getPort());
		client = new UbiquityClientBuilder().api(URL).build();
	}

	@Test
	public void shouldReadBtcPlatformInfo() throws ApiException, IOException {
		mockBackEnd.enqueue(new MockResponse().setBody(readJsonFile("btc_platforminfo.json")).addHeader("Content-Type",
				"application/json"));

		PlatformDetail details = client.platform().getPlatform(Platform.BITCOIN, Network.MAIN_NET);

		assertThat(details.getHandle(), is("bitcoin"));
		assertThat(details.getSource(), is("Ubiquity Database"));
		assertThat(details.getGenesisNumber(), is(0L));
		assertThat(details.getEndpoints(), hasItems("/account/:address/report", "/account/:address/txs", "/block/:id",
				"/block/:number", "/sync/block_id", "/sync/block_number", "/tx/:id", "/txs"));
	}

	@Test
	public void shouldReadPlatformsOverview() throws ApiException, IOException {
		mockBackEnd.enqueue(new MockResponse().setBody(readJsonFile("platforms_overview.json")).addHeader("Content-Type",
				"application/json"));

		PlatformsOverview details = client.platform().getPlatforms();
		List<PlatformsOverviewPlatforms> platforms = details.getPlatforms();
		int platformsSize = platforms.size();

		PlatformsOverviewPlatforms firstPlatform = platforms.get(0);
		PlatformsOverviewPlatforms lastPlatform = platforms.get(platformsSize - 1);

		assertThat(platformsSize, is(13));

		assertThat(firstPlatform.getHandle(), is("algorand"));
		assertThat(firstPlatform.getNetwork(), is("mainnet"));

		assertThat(lastPlatform.getHandle(), is("xrp"));
		assertThat(lastPlatform.getNetwork(), is("mainnet"));
	}

	@Test
	public void shouldReadBtcCurrentBlockHeight() throws ApiException, IOException {
		mockBackEnd.enqueue(new MockResponse().setBody(readJsonFile("btc_current_block_height.json"))
				.addHeader("Content-Type", "application/json"));

		UbiquityClient client = new UbiquityClientBuilder().api(URL).build();

		Long height = client.sync().currentBlockNumber(Platform.BITCOIN, Network.MAIN_NET);

		assertThat(height, is(685955l));
	}

	@Test
	public void shouldReadBtcCurrentBlockId() throws ApiException, IOException {
		mockBackEnd.enqueue(new MockResponse().setBody(readJsonFile("btc_current_block_id.json"))
				.addHeader("Content-Type", "application/json"));

		UbiquityClient client = new UbiquityClientBuilder().api(URL).build();

		String id = client.sync().currentBlockID(Platform.BITCOIN, Network.MAIN_NET);

		assertThat(id, is("0000000000000000000cb123a7e539a7d4ea04c6053d243382cde5c46d050898"));
	}

	@Test
	public void shouldReadBtcBlock() throws ApiException, IOException {
		mockBackEnd.enqueue(new MockResponse().setBody(readJsonFile("btc_block.json")).addHeader("Content-Type",
				"application/json"));

		UbiquityClient client = new UbiquityClientBuilder().api(URL).build();
		Block block = client.block().getBlock(Platform.BITCOIN, Network.MAIN_NET, 685700);

		assertThat(block.getId(), is("00000000000000000001a031c7ff632e6a8c1d95852468aaa17d8cacde17b6de"));
		assertThat(block.getNumber(), is(685700l));
		assertThat(block.getTxIds().size(), is(649));
		assertThat(block.getTxs().size(), is(1));
		
		MultiTransfer transfer = block.getTxs().get(0).getOperations().get("script").getMultiTransferOperation().getDetail();
		assertThat(transfer.getInputs(), hasItem( new Utxo().address("coinbase").value("625000000").assetPath("bitcoin/native/btc")));
		assertThat(transfer.getOutputs(), hasItem( new Utxo().address("18cBEMRxXHqzWWCxZNtU91F5sbUNKhL5PX").value("638354752").assetPath("bitcoin/native/btc")));
		
		assertThat(transfer.getTotalIn(), is("625000000"));
		assertThat(transfer.getTotalOut(), is("0"));
		assertThat(transfer.getUnspent(), is("625000000"));

		NativeCurrency currency = transfer.getCurrency().getNativeCurrency();
		assertThat(currency.getAssetPath(), is("bitcoin/native/btc"));
		assertThat(currency.getType(), is("native"));
		assertThat(currency.getDecimals(), is(8));
	}

	@Test
	public void shouldReadEthBlock() throws ApiException, IOException {
		mockBackEnd.enqueue(new MockResponse().setBody(readJsonFile("eth_block.json")).addHeader("Content-Type",
				"application/json"));

		UbiquityClient client = new UbiquityClientBuilder().api(URL).build();

		Block block = client.block().getBlock(Platform.ETHEREUM, Network.MAIN_NET, "current");

		assertThat(block.getId(), is("0x8e4874a0f4cbadb8052e68b58f4780bb7009bf6ce2519f18bbadc0052616bb3f"));
		assertThat(block.getNumber(), is(12548882l));
		assertThat(block.getTxIds().size(), is(225));
		assertThat(block.getTxs().size(), is(225));
		
		Transfer transfer = block.getTxs().get(0).getOperations().get("log-1-transfer").getTransferOperation().getDetail();
		assertThat(transfer.getTo(), is("0x87535b160E251167FB7abE239d2467d1127219E4"));
		assertThat(transfer.getFrom(), is("0x69a263804BC8Ea966095eA21aea58b9eA6Bf52A7"));
		
		SmartTokenCurrency currency = transfer.getCurrency().getSmartTokenCurrency();
		assertThat(currency.getAssetPath(), is("ethereum/contract/0xd23ac27148af6a2f339bd82d0e3cff380b5093de/erc-20"));
		assertThat(currency.getDetail().getType(), is("ERC-20"));
		assertThat(currency.getDetail().getContract(), is("0xD23Ac27148aF6A2f339BD82D0e3CFF380b5093de"));
	}

	@Test
	public void shouldReadAlgorandAccountBalance() throws ApiException, IOException {
		mockBackEnd.enqueue(new MockResponse().setBody(readJsonFile("algo_account_balance.json"))
				.addHeader("Content-Type", "application/json"));

		UbiquityClient client = new UbiquityClientBuilder().api(URL).build();

		Map<String, Object> balances = client.accounts().getBalancesByAddress(Platform.name("algorand"),
				Network.MAIN_NET, "HG2JL36OPPITBA7RNIPW4GUQS74AF3SEBO6DAJSLJC33C34I2DQ42F5MU4");
		Map<String, Object> balance = (Map<String, Object>) balances.get("algorand/native/algo");

		assertThat(balance.get("balance"), is("175127831"));
		assertThat(balance.get("currency"), notNullValue());
	}

	@Test
	public void shouldReadEthAccountTxs() throws ApiException, IOException {
		mockBackEnd.enqueue(new MockResponse().setBody(readJsonFile("eth_address_txs.json")).addHeader("Content-Type",
				"application/json"));

		UbiquityClient client = new UbiquityClientBuilder().api(URL).build();

		TxPage txPage = client.accounts().getTxsByAddress(Platform.ETHEREUM, Network.MAIN_NET,
				"0x49bC2A9EE1A08dbCa7dd66629700E68AA8DB09aC", "");

		assertThat(txPage.getContinuation(), nullValue());
		assertThat(txPage.getTotal(), is(4));
		assertThat(txPage.getItems().get(0).getId(),
				is("0x24d78b7371bb12c29ac799492dc891331036884ef3448ff7883cddfa02dd0bbe"));
	}

	@Test
	public void shouldReadEthTx() throws ApiException, IOException {
		mockBackEnd.enqueue(
				new MockResponse().setBody(readJsonFile("eth_tx.json")).addHeader("Content-Type", "application/json"));

		UbiquityClient client = new UbiquityClientBuilder().api(URL).build();

		Tx tx = client.transactions().getTx(Platform.name("ethereum"), Network.name("mainnet"),
				"0x6821b32162ad40f979ad8e999ffbe358e5df0f54e1894d1b3fc3e01fce6a134b");

		assertThat(tx.getBlockId(), is("0x9b519a6925c2f4118f952073d5cca342294ecea9f6b8517eb8288be61589b86a"));
		assertThat(tx.getAddresses(), hasItems("0x49bC2A9EE1A08dbCa7dd66629700E68AA8DB09aC",
				"0xA090e606E30bD747d4E6245a1517EbE430F0057e", "transaction_fees"));
	}

	@Test
	public void shouldReadDiemTxPages() throws ApiException, IOException {
		mockBackEnd.enqueue(new MockResponse().setBody(readJsonFile("diem_tx_page_1.json")).addHeader("Content-Type",
				"application/json"));
		mockBackEnd.enqueue(new MockResponse().setBody(readJsonFile("diem_tx_page_2.json")).addHeader("Content-Type",
				"application/json"));

		UbiquityClient client = new UbiquityClientBuilder().api(URL).build();

		TxPage txPage1 = client.transactions().getTxs(Platform.DIEM, Network.MAIN_NET, Order.DESC, "", null, null);

		assertThat(txPage1.getContinuation(), is("24"));
		assertThat(txPage1.getTotal(), is(25));
		assertThat(txPage1.getItems().get(0).getId(),
				is("038c06e3074435a689e5227467cc61fdfdc364df70ff24f24713ed5d777b2313"));

		TxPage txPage2 = client.transactions().getTxs(Platform.DIEM, Network.MAIN_NET, Order.DESC,
				txPage1.getContinuation());

		assertThat(txPage2.getContinuation(), is("49"));
		assertThat(txPage2.getTotal(), is(25));
		assertThat(txPage2.getItems().get(0).getId(),
				is("9aaf5461b1ac02f8e2b3aa17f6ffd30549be5ad720e556bdb3b588537b29c130"));
		assertThat(txPage2.getItems().get(0).getDate(), is(1611693920405957l));
	}

	@AfterClass
	public static void tearDown() throws IOException {
		mockBackEnd.shutdown();
	}

}
