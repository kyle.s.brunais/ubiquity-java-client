# AccountsApi

All URIs are relative to *https://ubiquity.api.blockdaemon.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getBalancesByAddress**](AccountsApi.md#getBalancesByAddress) | **GET** /{platform}/{network}/account/{address} | Balances Of Address
[**getReportByAddress**](AccountsApi.md#getReportByAddress) | **GET** /{platform}/{network}/account/{address}/report | A financial report for an address between a time period. Default timescale is within the last 30 days
[**getTxsByAddress**](AccountsApi.md#getTxsByAddress) | **GET** /{platform}/{network}/account/{address}/txs | Transactions Of Address



## getBalancesByAddress

> Map&lt;String, Object&gt; getBalancesByAddress(platform, network, address)

Balances Of Address

Returns the account balances for all supported currencies.


### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.AccountsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com/v2");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        AccountsApi apiInstance = new AccountsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        String address = "0x2E31B312290A01538514806Fbb857736ea4d5555"; // String | Account address
        try {
            Map<String, Object> result = apiInstance.getBalancesByAddress(platform, network, address);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AccountsApi#getBalancesByAddress");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |
 **address** | **String**| Account address |

### Return type

**Map&lt;String, Object&gt;**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Balances |  -  |
| **400** | Bad request |  -  |
| **401** | Invalid or expired token |  -  |
| **429** | Rate limit exceeded |  -  |


## getReportByAddress

> Report getReportByAddress(platform, network, address, from, to)

A financial report for an address between a time period. Default timescale is within the last 30 days

Returns account activity


### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.AccountsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com/v2");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        AccountsApi apiInstance = new AccountsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        String address = "0x2E31B312290A01538514806Fbb857736ea4d5555"; // String | Account address
        Integer from = 961846434; // Integer | Unix Timestamp from where to start
        Integer to = 1119612834; // Integer | Unix Timestamp from where to end
        try {
            Report result = apiInstance.getReportByAddress(platform, network, address, from, to);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AccountsApi#getReportByAddress");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |
 **address** | **String**| Account address |
 **from** | **Integer**| Unix Timestamp from where to start | [optional]
 **to** | **Integer**| Unix Timestamp from where to end | [optional]

### Return type

[**Report**](Report.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, text/csv, application/problem+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Account Activity |  -  |
| **400** | Bad request |  -  |
| **401** | Invalid or expired token |  -  |
| **429** | Rate limit exceeded |  -  |
| **413** | Too Many Transactions |  -  |


## getTxsByAddress

> TxPage getTxsByAddress(platform, network, address, order, continuation, limit)

Transactions Of Address

Gets transactions that an address was involved with, from newest to oldest.
This call uses pagination.


### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.AccountsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com/v2");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        AccountsApi apiInstance = new AccountsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        String address = "0x2E31B312290A01538514806Fbb857736ea4d5555"; // String | Account address
        String order = "order_example"; // String | Pagination order
        String continuation = "8185.123"; // String | Continuation token from earlier response
        Integer limit = 25; // Integer | Max number of items to return in a response. Defaults to 25 and is capped at 100. 
        try {
            TxPage result = apiInstance.getTxsByAddress(platform, network, address, order, continuation, limit);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AccountsApi#getTxsByAddress");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |
 **address** | **String**| Account address |
 **order** | **String**| Pagination order | [optional] [enum: desc, asc]
 **continuation** | **String**| Continuation token from earlier response | [optional]
 **limit** | **Integer**| Max number of items to return in a response. Defaults to 25 and is capped at 100.  | [optional]

### Return type

[**TxPage**](TxPage.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Transactions |  -  |
| **400** | Invalid address |  -  |
| **401** | Invalid or expired token |  -  |
| **403** | Invalid continuation |  -  |
| **429** | Rate limit exceeded |  -  |

