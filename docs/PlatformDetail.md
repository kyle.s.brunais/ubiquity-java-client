

# PlatformDetail


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source** | **String** | Backend API Type |  [optional]
**handle** | **String** |  |  [optional]
**genesisNumber** | **Long** |  |  [optional]
**endpoints** | **List&lt;String&gt;** |  |  [optional]



